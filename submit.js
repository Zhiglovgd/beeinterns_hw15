const form = document.querySelector('#myForm');
const formMessage = document.querySelector('.form-message');

form.addEventListener('submit', (e) => {
  e.preventDefault();

  const xhr = new XMLHttpRequest();
  const data = new FormData(form);

  data.append('newField', 'customValue');
  data.delete('hiddenInput');

  xhr.open('POST', "posturl/index.json", true);

  xhr.onloadend = () => {
    if (xhr.status === 200) {
      form.reset();
      formMessage.classList.toggle('form-message_unactive');
      setTimeout(() => formMessage.classList.toggle('form-message_unactive'), 10000);
      console.log(`Успех! ${xhr.status}`);
    } else {
      alert("Ошибка отправления " + xhr.status);
    }
  };

  xhr.send(data);
});